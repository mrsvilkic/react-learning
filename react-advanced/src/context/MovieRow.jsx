import React, { useContext } from "react";
import UserContext from "./userContext";

function MovieRow(props) {
  const userContext = useContext(UserContext);

  return (
    <div>
      <p>{userContext.currentUser ? userContext.currentUser.name : ""}</p>
      <button onClick={userContext.onLoggedIn}>login</button>
    </div>
  );
}

export default MovieRow;
