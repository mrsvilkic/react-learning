import React, { Component } from "react";
import UserContext from "./userContext";
import MovieRow from "./MovieRow";

class MovieList extends Component {
  static contextType = UserContext;

  componentDidMount() {
    console.log("contex", this.context);
  }

  render() {
    return <MovieRow />;
  }
}

export default MovieList;
