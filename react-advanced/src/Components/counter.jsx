import React, { useState } from "react";
import useDocumentTitle from "../hooks/useDocumentTitle";

function Counter(props) {
  const [count, setCount] = useState(0);
  const [text, setText] = useState("");

  useDocumentTitle(`${text} clicked + ${count} times`);

  return (
    <div>
      <input
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <p>
        {text} clicked + {count} times
      </p>
      <button onClick={() => setCount(count + 1)}>+</button>
    </div>
  );
}

export default Counter;
