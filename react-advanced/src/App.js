import React, { Component } from "react";
import MoviePage from "./context/MoviePage";
import UserContext from "./context/userContext";

class App extends Component {
  state = {
    currentUser: { name: null },
  };

  handleLoggedIn = (username) => {
    console.log("Getting the user" + username);
    const user = { name: "Aleksa" };
    this.setState({ currentUser: user });
  };

  render() {
    return (
      <UserContext.Provider
        value={{
          currentUser: this.state.currentUser,
          onLoggedIn: this.handleLoggedIn,
        }}
      >
        <MoviePage />
      </UserContext.Provider>
    );
  }
}

export default App;
