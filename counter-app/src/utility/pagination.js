export const pagination = (items, page, itemsPerPage) => {
  const split = [];
  const start = (page - 1) * itemsPerPage;

  for (let i = start; i < start + itemsPerPage; i++) {
    if (items[i]) split.push(items[i]);
  }
  return split;
};
