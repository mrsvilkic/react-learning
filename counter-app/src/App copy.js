import React, { Component } from "react";
import Counters from "./components/counters";
import NavBar from "./components/navbar";
import Movies from "./components/movies";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 4, disabled: false },
      { id: 2, value: 0, disabled: true },
      { id: 3, value: 0, disabled: true },
      { id: 4, value: 2, disabled: false },
    ],
  };

  render() {
    return (
      <>
        <div className="container">
          <NavBar
            counters={this.state.counters.filter((c) => c.value > 0).length}
          />
          <div className="row">
            <Counters
              counters={this.state.counters}
              onDelete={this.handleDelete}
              onReset={this.handleReset}
              onIncrement={this.handleIncrement}
              onDecrement={this.handleDecrement}
            />
          </div>
        </div>
      </>
    );
  }

  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index].value += 1;
    if (counters[index].value > 0) counters[index].disabled = false;
    this.setState({ counters });
  };

  handleDecrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    if (counters[index].value - 1 >= 0) {
      counters[index].value -= 1;
      if (counters[index].value === 0) counters[index].disabled = true;
      this.setState({ counters });
    }
  };

  handleDelete = (id) => {
    let counters = this.state.counters.filter((counter) => counter.id !== id);
    this.setState({ counters });
  };
}

export default App;
