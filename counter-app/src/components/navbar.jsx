import React from "react";
import { NavLink } from "react-router-dom";

const NavBar = ({ user }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light mb-3">
      <NavLink className="navbar-brand" to="/">
        Filmovita
      </NavLink>
      <NavLink className="nav-item nav-link " to="/movies">
        Movies
      </NavLink>
      <NavLink className="nav-item nav-link " to="/customers">
        Customers
      </NavLink>
      <NavLink className="nav-item nav-link " to="/rentals">
        Rentals
      </NavLink>
      {user && (
        <>
          <NavLink className="nav-item nav-link " to="/profile">
            {user.name}
          </NavLink>
          <NavLink className="nav-item nav-link " to="/logout">
            Logout
          </NavLink>
        </>
      )}
      {!user && (
        <>
          <NavLink className="nav-item nav-link " to="/login">
            Login
          </NavLink>
          <NavLink className="nav-item nav-link " to="/register">
            Register
          </NavLink>
        </>
      )}
    </nav>
  );
};

export default NavBar;
