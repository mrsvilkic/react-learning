import React from "react";
import Joi from "joi-browser";
import { toast } from "react-toastify";

import Form from "./common/form";
import { registerUser } from "../services/userService";
import { loginWithJwt } from "../services/authService";
class RegisterForm extends Form {
  state = {
    data: { email: "", password: "", name: "" },
    errors: {},
  };

  schema = {
    email: Joi.string().email().required().label("Username"),
    password: Joi.string().min(3).required().label("Password"),
    name: Joi.string().required().label("Name"),
  };

  doSubmit = async () => {
    try {
      //Register
      const response = await registerUser(this.state.data);
      loginWithJwt(response.headers["x-auth-token"]);
      toast("Succesfully registerd!");
      window.location = "/";
    } catch (error) {
      if (error.response && error.response.status === 400) {
        const errors = { ...this.state.errors };
        errors.email = error.response.data;
        this.setState({ errors });
        // toast.error("User already exists");
      }
    }
  };

  render() {
    return (
      <div className="d-flex justify-content-center">
        <div className="col col-6">
          <h1>Register</h1>
          <form onSubmit={this.handleSubmit}>
            {this.renderInput("email", "Username", "email")}
            {this.renderInput("password", "Password", "password")}
            {this.renderInput("name", "Name")}
            {this.renderButton("Register")}
          </form>
        </div>
      </div>
    );
  }
}

export default RegisterForm;
