import React, { Component } from "react";
import { toast } from "react-toastify";
import { getMovies, deleteMovie } from "../services/movieService";
import { getGenres } from "../services/genreService";
import { pagination } from "../utility/pagination";
import { NavLink } from "react-router-dom";
import Paging from "./common/paging";
import ListGroup from "./common/listgroup";
import MoviesTable from "./moviesTable";
import SearchBox from "./common/searchBox";

class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    currentPage: 1,
    moviesPerPage: 4,
    search: "",
    selectedGenre: null,
    sortColumn: { path: "title", order: 1 },
  };

  async componentDidMount() {
    const { data: genres } = await getGenres();
    const { data: movies } = await getMovies();
    this.setState({ movies });
    this.setState({ genres });
  }

  render() {
    const {
      currentPage,
      genres,
      moviesPerPage,
      selectedGenre,
      sortColumn,
    } = this.state;
    const { user } = this.props;

    const { totalCount, data: movies } = this.getPagedData();

    return (
      <div className="row">
        <ListGroup
          items={genres}
          selectedItem={selectedGenre}
          onItemSelect={this.handleGenreChange}
        />
        <div className="col col-9">
          {user && (
            <NavLink className="btn btn-primary" to="/movies/new">
              Add Movie
            </NavLink>
          )}

          <SearchBox onChange={this.handleSearch} value={this.state.search} />

          {totalCount === 0 ? (
            <p>There are no movies</p>
          ) : (
            <>
              <p>Showing {totalCount} movies in database</p>
              <MoviesTable
                movies={movies}
                onDelete={this.handleDelete}
                onLike={this.handleLike}
                onSort={this.handleSort}
                sortBy={sortColumn}
              />
              <Paging
                currentPage={currentPage}
                itemCount={totalCount}
                itemsPerPage={moviesPerPage}
                onPageClick={this.handlePageChange}
              />
            </>
          )}
        </div>
      </div>
    );
  }

  getPagedData = () => {
    const {
      currentPage,
      movies: AllMovies,
      moviesPerPage,
      selectedGenre,
      sortColumn,
      search,
    } = this.state;

    let filtered;

    if (search.trim() !== "")
      filtered = AllMovies.filter((movie) =>
        movie.title.toLowerCase().includes(search)
      );
    else
      filtered = selectedGenre
        ? AllMovies.filter((movie) => movie.genre._id === selectedGenre._id)
        : AllMovies;

    if (sortColumn.path === "genre.name") {
      filtered = [...filtered].sort((a, b) =>
        a.genre.name > b.genre.name
          ? 1 * sortColumn.order
          : -1 * sortColumn.order
      );
    } else {
      filtered = [...filtered].sort((a, b) =>
        a[sortColumn.path] > b[sortColumn.path]
          ? 1 * sortColumn.order
          : -1 * sortColumn.order
      );
    }
    const movies = pagination(filtered, currentPage, moviesPerPage);

    return { totalCount: filtered.length, data: movies };
  };

  handleSort = (path) => {
    const obj = { path, order: this.state.sortColumn.order * -1 };
    this.setState({ sortColumn: obj });
  };

  handleSearch = (text) => {
    this.setState({
      search: text.toLowerCase(),
      selectedGenre: null,
      currentPage: 1,
    });
  };

  handleGenreChange = (genre) => {
    this.setState({ selectedGenre: genre, search: "", currentPage: 1 });
  };

  handlePageChange = (page) => {
    this.setState({ currentPage: page });
  };

  handleDelete = async (movie) => {
    const originalMovies = this.state.movies;
    this.setState({ movies: this.state.movies.filter((m) => m !== movie) });
    try {
      await deleteMovie(movie._id);
    } catch (ex) {
      this.setState({ movies: originalMovies });
      if (ex.response && ex.response.status === 404)
        toast.error("Movie already deleted!");
    }
  };

  handleLike = (movie) => {
    const movies = [...this.state.movies];
    const index = movies.indexOf(movie);
    movies[index].liked = !movies[index].liked;
    this.setState({ movies });
  };
}

export default Movies;
