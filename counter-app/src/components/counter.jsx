import React, { Component } from "react";

class Counter extends Component {
  render() {
    const { onDelete, onIncrement, counter, onDecrement } = this.props;
    return (
      <div className="container">
        <span className={this.getBadgeClasses()}> {this.formatCount()}</span>
        <button
          onClick={() => onIncrement(counter)}
          className="btn btn-secondary btn-sm mr-2"
        >
          +
        </button>
        <button
          onClick={() => onDecrement(counter)}
          className="btn btn-secondary btn-sm mr-2"
          disabled={counter.disabled}
        >
          -
        </button>
        <button
          onClick={() => onDelete(counter.id)}
          className="btn btn-danger btn-sm"
        >
          /
        </button>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "col-1 badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount = () => {
    const { value: count } = this.props.counter;
    return count === 0 ? "zero" : count;
  };
}

export default Counter;
