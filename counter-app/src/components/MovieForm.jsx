import React from "react";
import Form from "./common/form";
import Joi from "joi-browser";
import { saveMovie, getMovie } from "../services/movieService";
import { getGenres } from "../services/genreService";
import { toast } from "react-toastify";
class MovieForm extends Form {
  state = {
    data: { title: "", genreId: "", numberInStock: 0, dailyRentalRate: 0 },
    genres: [],
    errors: {},
  };

  schema = {
    _id: Joi.string(),
    title: Joi.string().required().label("Title"),
    genreId: Joi.string().required().label("Genre"),
    numberInStock: Joi.number().min(0).label("Number In Stock"),
    dailyRentalRate: Joi.number().min(0).max(10).required().label("Rate"),
  };

  async componentDidMount() {
    await this.populateGenres();
    await this.populateMovie();
  }

  populateGenres = async () => {
    const { data: genres } = await getGenres();
    this.setState({ genres });
  };

  populateMovie = async () => {
    try {
      const movieID = this.props.match.params.id;
      if (movieID === "new") return;
      const { data: movie } = await getMovie(movieID);
      this.setState({ data: this.mapToViewModel(movie) });
    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status < 500
      ) {
        toast.error("Movie is not available!");
        this.props.history.replace("/not-found");
      }
    }
  };

  mapToViewModel(movie) {
    return {
      _id: movie._id,
      title: movie.title,
      genreId: movie.genre._id,
      numberInStock: movie.numberInStock,
      dailyRentalRate: movie.dailyRentalRate,
    };
  }

  doSubmit = () => {
    saveMovie(this.state.data);
    this.props.history.push("/movies");
  };

  render() {
    // const genres = getGenres();
    return (
      <div className="d-flex justify-content-center">
        <div className="col col-6">
          <h1>Add Movie</h1>
          <form onSubmit={this.handleSubmit}>
            {this.renderInput("title", "Title")}
            {this.renderSelect("genreId", "Genre", this.state.genres)}
            {this.renderInput("numberInStock", "Number In Stock", "number")}
            {this.renderInput("dailyRentalRate", "Rate")}
            {this.renderButton("Add  Movie")}
          </form>
        </div>
      </div>
    );
  }
}

export default MovieForm;
