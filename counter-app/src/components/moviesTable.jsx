import React, { Component } from "react";
import Like from "./common/like";
import Table from "./common/table";
import { Link } from "react-router-dom";
import auth from "../services/authService";

class MoviesTable extends Component {
  columns = [
    {
      label: "Naziv",
      path: "title",
      content: (movie) => (
        <Link to={`/movies/${movie._id}`}>{movie.title}</Link>
      ),
    },
    { label: "Žanr", path: "genre.name" },
    { label: "Količina", path: "numberInStock" },
    { label: "Ocena", path: "dailyRentalRate" },
    {
      label: "Fav",
      content: (movie) => (
        <Like liked={movie.liked} onLike={() => this.props.onLike(movie)} />
      ),
    },
  ];

  deletColumn = {
    label: "Opcije",
    content: (movie) => (
      <button
        onClick={() => this.props.onDelete(movie)}
        className="btn btn-danger btn-sm"
      >
        Delete
      </button>
    ),
  };

  constructor() {
    super();
    const user = auth.getCurrentUser();
    if (user && user.isAdmin) this.columns.push(this.deletColumn);
  }

  render() {
    const { movies, sortBy, onLike, onDelete, onSort } = this.props;
    return (
      <Table
        data={movies}
        columns={this.columns}
        onLike={onLike}
        onDelete={onDelete}
        onSort={onSort}
        sortBy={sortBy}
      />
    );
  }
}

export default MoviesTable;
