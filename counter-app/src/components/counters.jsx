import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    const {
      counters,
      onDelete,
      onDecrement,
      onIncrement,
      onReset,
    } = this.props;
    return (
      <div className="container">
        <button className="btn btn-primary btn-sm" onClick={onReset}>
          Reset
        </button>
        <div className="row">
          {counters.map((counter) => (
            <Counter
              key={counter.id}
              counter={counter}
              onDelete={onDelete}
              onIncrement={onIncrement}
              onDecrement={onDecrement}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Counters;
