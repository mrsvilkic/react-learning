import React, { Component } from "react";
import PropTypes from "prop-types";

class Paging extends Component {
  render() {
    const { currentPage, itemCount, itemsPerPage, onPageClick } = this.props;
    const numberOfPages = Math.ceil(itemCount / itemsPerPage);

    if (numberOfPages === 1) return null;

    //[1,2, ... numberOfPages]
    const numbers = Array(numberOfPages)
      .fill()
      .map((e, i) => i + 1);

    return (
      <ul className="pagination d-flex justify-content-center">
        {numbers.map((number) => (
          <li
            onClick={() => onPageClick(number)}
            key={number}
            className={
              number === currentPage ? "page-item active" : "page-item"
            }
          >
            <a style={{ cursor: "pointer" }} className="page-link">
              {number}
            </a>
          </li>
        ))}
      </ul>
    );
  }
}

Paging.propTypes = {
  currentPage: PropTypes.number.isRequired,
  itemCount: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  onPageClick: PropTypes.func.isRequired,
};

export default Paging;
