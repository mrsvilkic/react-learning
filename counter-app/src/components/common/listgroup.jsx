import React, { Component } from "react";

class ListGroup extends Component {
  state = {};
  render() {
    const {
      items,
      onItemSelect,
      indexProperty,
      selectedItem,
      textProperty,
    } = this.props;

    return (
      <div className="col col-3">
        <ul className="list-group">
          <li
            onClick={() => onItemSelect(null)}
            className={
              null === selectedItem
                ? "list-group-item active"
                : "list-group-item"
            }
            style={{ cursor: "pointer" }}
          >
            Filter All
          </li>
          {items.map((item) => (
            <li
              onClick={() => onItemSelect(item)}
              key={item[indexProperty]}
              className={
                item === selectedItem
                  ? "list-group-item active"
                  : "list-group-item"
              }
              style={{ cursor: "pointer" }}
            >
              {item[textProperty]}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

ListGroup.defaultProps = {
  indexProperty: "_id",
  textProperty: "name",
};

export default ListGroup;
