import React, { Component } from "react";
import { FaSortDown, FaSortUp } from "react-icons/fa";

//label array
//sortBy
//onSort
class TableHeader extends Component {
  render() {
    return (
      <thead>
        <tr>
          {this.props.columns.map((column) => (
            <th
              key={column.label}
              style={{ cursor: "pointer" }}
              onClick={() => this.props.onSort(column.path)}
            >
              {column.label}
              {column.path === this.props.sortBy.path ? (
                this.props.sortBy.order === 1 ? (
                  <FaSortUp />
                ) : (
                  <FaSortDown />
                )
              ) : (
                ""
              )}
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
