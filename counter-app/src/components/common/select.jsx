import React from "react";

const Select = ({ name, label, options, error, value, ...rest }) => {
  //   console.log("props", props);
  return (
    <div className="form-group">
      <label htmlFor={name}>{label}</label>
      <select
        className="form-control"
        value={value}
        name={name}
        id={name}
        {...rest}
      >
        <option value=""></option>
        {options.map((option) => (
          <option value={option._id} key={option._id}>
            {option.name}
          </option>
        ))}
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </div>
  );
};

export default Select;
