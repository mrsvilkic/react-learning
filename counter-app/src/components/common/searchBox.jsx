import React from "react";
import Input from "./input";

function SearchBox({ value, onChange }) {
  return (
    <Input
      type="text"
      name="query"
      placeholder="Search..."
      value={value}
      onChange={(e) => onChange(e.currentTarget.value)}
    />
  );
}

export default SearchBox;
