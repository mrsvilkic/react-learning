import TableHeader from "./tableHeader";
import TableBody from "./tableBody";
import React from "react";

function table({ data, columns, onSort, sortBy, onLike, onDelete }) {
  return (
    <table className="table ">
      <TableHeader columns={columns} onSort={onSort} sortBy={sortBy} />
      <TableBody
        data={data}
        columns={columns}
        onLike={onLike}
        onDelete={onDelete}
      />
    </table>
  );
}

export default table;
