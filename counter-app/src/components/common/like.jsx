import React from "react";

import { FaRegHeart, FaHeart } from "react-icons/fa";

const Like = ({ liked, onLike }) => {
  return liked ? (
    <FaHeart style={{ cursor: "pointer" }} onClick={onLike} />
  ) : (
    <FaRegHeart style={{ cursor: "pointer" }} onClick={onLike} />
  );
};

export default Like;
