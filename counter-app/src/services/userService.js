import http from "./httpService";

const apiEnpoint = "http://localhost:3900/api/users";

export const registerUser = (user) => {
  return http.post(apiEnpoint, user);
};
