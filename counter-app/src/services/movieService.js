import http from "./httpService";
import { apiUrl } from "../config.json";

const apiEndpoint = apiUrl + "/movies";

const movieUrl = (movieId) => `${apiEndpoint}/${movieId}`;

export function getMovies() {
  return http.get(apiEndpoint);
}

export function getMovie(movieId) {
  return http.get(movieUrl(movieId));
}

export function saveMovie(movie) {
  // Update
  if (movie._id) {
    const body = { ...movie };
    delete body._id;
    return http.put(movieUrl(movie._id), body);
  }
  // Add new
  return http.post(apiEndpoint, movie);
}

export function deleteMovie(movieId) {
  http.delete(movieUrl(movieId));
}
