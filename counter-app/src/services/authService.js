import http from "./httpService";
import jwtDecode from "jwt-decode";

const apiEndpoint = "http://localhost:3900/api/auth";
const token = "token";

http.setJwt(getJwt());

export async function login(user) {
  const { data: jwt } = await http.post(apiEndpoint, user);
  localStorage.setItem(token, jwt);
}
export function loginWithJwt(jwt) {
  localStorage.setItem(token, jwt);
}
export function logout() {
  localStorage.removeItem(token);
}

export function getJwt() {
  return localStorage.getItem(token);
}

export function getCurrentUser() {
  try {
    const jwt = localStorage.getItem(token);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}

export default {
  login,
  logout,
  getJwt,
  loginWithJwt,
  getCurrentUser,
};
