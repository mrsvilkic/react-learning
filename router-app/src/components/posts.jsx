import React from "react";
import queryString from "query-string";

const Posts = ({ match, location }) => {
  const result = queryString.parse(location.search);

  return (
    <div>
      <h1>Posts</h1>
      Year:{result.year} , Month:{result.month}
    </div>
  );
};

export default Posts;
