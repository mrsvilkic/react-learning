import React, { Component } from "react";
import { Link } from "react-router-dom";

class AdminSideBar extends Component {
  state = {};
  render() {
    return (
      <ul>
        <li>
          <Link to="/admin/users">Users</Link>
        </li>
        <li>
          <Link to="/admin/posts">Posts</Link>
        </li>
      </ul>
    );
  }
}

export default AdminSideBar;
